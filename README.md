# build-tools

This repository hosts our scripts and tooling used by our Jenkins CI system to build and provide Debian packages to be published at http://repo.ubports.com/. It also hosts scripts used to manage system-image OTA image promotion between channels.

## Debian package building on our CI

Our CI system works like this:

- Each repository will contain a boilerplate [Jenkinsfile] which includes our common Jenkins script [buildAndProvideDebianPackage.groovy]. The file will be placed at `debian/Jenkinsfile` to avoid putting packaging-related files outside of `debian/` folder.
- CI will periodically scan repositories under [ubports/development/core] GitLab group, and will automatically add any repository which contains `debian/Jenkinsfile` into Jenkins.
- The script works together with the rest of the code in this repository to build and provide packages to http://repo.ubports.com/. For each branch, [ut-version-info.json] controls which Ubuntu (or Debian) versions it will build the code against and which APT archives it will publish the built package(s) to.

> **Note:** `ut-version-info.json` also contains other metadata related to how we build Ubuntu Touch images, and may be interesting to other developers wanting to integrate into our APT archive.

[Jenkinsfile]: ./Jenkinsfile
[buildAndProvideDebianPackage.groovy]: ./vars/buildAndProvideDebianPackage.groovy
[ubports/development/core]: https://gitlab.com/ubports/development/core
[ut-version-info.json]: ./ut-version-info.json

### Build configurations in Jenkinsfile

`buildAndProvideDebianPackage()` accepts a number of arguments via Jenkinsfile.

- isArchIndependent: a hint that this package contains only 'Architecture:
  all' package(s), and thus Jenkins doesn't have to dispatch build to nodes
  of every architecture. Note that this will confuse BlueOcean UI, causing it
  to not show any progress during the 'Build binary' step. However, if
  needed, one can still track the progress on the classic UI.
- ignoredArches: a list of architectures where the package should not be
  built. For example, [ 'armhf' ].
- isHeavyPackage: a hint that this package requires a significantly more
  resource to build, and would benefit from building on faster nodes.

### Build configuration files in `debian/`

Certain aspects of package building can be configured through a set of files named `ubports.*` in `debian/` folder.

> Note: The CI also reads these files at the top level folder. However, this is deprecated as 1.) they clutter the source directory unnecessarily and 2.) for packaging repos, except for `debian/` folder, other files (if exist) must come from the orig tarball.

The following files are supported:

- `ubports.source_location`: specifies location(s) to pull orig source tarball(s) from, and signify that this package is a "packaging repository" (see [handling of "packaging repositories"] below).
- `ubports.no_test`: enable `nocheck` build option and build profile, resulting in no tests being run.
- `ubports.no_doc`: enable `nodoc` build option and build profile, which will disable building documentation in packages that support it.
- `ubports.build_profiles`: apply additional build profiles specified in the file. This is useful in bootstrapping circular dependencies. Each build profiles are separated by a whitespace.
- `ubports.backports`: enable the backport APT archive from the base distro.
- `ubports.architecture`: specifies architectures which this package should be built on.
- `ubports.skip_distro`: specifies "distribution" in which package building will be skipped. "Distribution" is defined as the keys of [`ut-version-info.json`]. Each distribution to skip is on its own line, with comment lines starting with `#`. This is intended to be used when the version of source code in the `main` branch is known to not be buildable on Debian and efforts to make it builds on Debian is too high.

[handling of "packaging repositories"]: #handling-of-packaging-repositories
[`ut-version-info.json`]: ./ut-version-info.json

### Handling of the MRs

In additional to branches defined in `ut-version-info.json`, Jenkins will also build any MRs targetting those branches and provide packages in their own, separated APT archive. The archive name will be in the format of `{APT archive of target branch}_-_PR_{GitLab repo name}_{MR number}`. The archive name can be subsequently used with [ubports-qa] command to install packages built from the MR for testing purpose.

If an MR depends on packages built from other MRs, one can temporarily add file `debian/ubports.depends`, with each line containing another MR in format of `PR_{GitLab repo name}_{MR number}` (CI will automatically prefix APT archive of target branch for you). To do this, the other MRs must target the same branch as this MR. Don't forget to remove such file after those MRs are merged, as the file will cause CI to fail when merged.

> **Note:** we're developing a system which allows you to specify other MRs inside the MR description to avoid forgetting to remove the file before merging. In the future, we also want to utilize GitLab's MR dependency system when they're exposed via an API (see [this GitLab epic]).

> **Note:** branch extension (in the form of e.g. `ubports/focal_-_something`) is another way to aggregate packages from other MRs for dependency purpose. However, due to its use no longer maps well with the new branching scheme, its use is deprecated. It will no longer be allowed when we no longer support Ubuntu Touch based on Ubuntu 20.04.

[ubports-qa]: https://gitlab.com/ubports/development/core/ubports-qa-scripts
[this epic]: https://gitlab.com/groups/gitlab-org/-/epics/8173

### Handling of "packaging repositories"

"Packaging repository" is defined as a Git repository which contains Debian packaging of another package not maintained by UBports, but used as part of Ubuntu Touch. This is distinguished by the presence of file `debian/ubports.source_location` which defines how CI will pull orig tarballs. The format of the file is:

```
[URL of orig tarball 1]
[File name of orig tarball 1]
[URL of orig tarball 2]
[File name of orig tarball 2]
...
```

A Debian package can contain 1 or more orig tarball, following [the handling of multiple orig tarballs] in Debian packaging. The version have to contain [`debian_revision`] (the part after dash in e.g. `1.0-0ubports1`), and the package have to be in `3.0 (quilt)` format (as opposed to `3.0 (native)` format used by packages maintained by UBports).

`3.0 (quilt)` format requires that any source code modifications must be a patch inside `debian/patches`. This is done to make sure we understand what modifications are made and where they are from. Learn more about how to make such a modification on [Debian wiki].

> **Note:** while it's tempting to just import upstream repository into our GitLab group and start modifying away, please don't. It makes it harder to upgrade version when the time comes.

> **Note:** while packaging repos usually don't contain the source code itself, you may have encountered some packaging repos which do. Make no mistake; if they contain `debian/ubports.source_location`, they are still packaging repos. Any customization has to still be done using patches in `debian/patches`. Do NOT modify files outside of `debian/`, or CI will fail to build the package.

[the handling of multiple orig tarballs]: https://raphaelhertzog.com/2010/09/07/how-to-use-multiple-upstream-tarballs-in-debian-source-packages/
[`debian_revision`]: https://www.debian.org/doc/debian-policy/ch-controlfields.html#version
[Debian wiki]: https://wiki.debian.org/UsingQuilt

## System-image OTA image promotion scripts

> **Note:** the scripts are not yet fully understood. More information will be added in the future.
