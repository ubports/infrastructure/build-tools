import org.jenkinsci.plugins.workflow.support.steps.build.RunWrapper

def call(
  RunWrapper currentBuild,
  String jobDescription,
  String jobUrl,
  Boolean wantTelegramNotification = true
) {
  String status;
  Boolean needUrl = false;
  String urlText, fullText, extendedComment;

  switch (currentBuild.currentResult) {
    case "SUCCESS":
      if (currentBuild?.getPreviousBuild()?.resultIsWorseOrEqualTo("UNSTABLE"))
        status = "FIXED";
      else
        status = "SUCCESS";

      break;

    case "UNSTABLE":
      status = "UNSTABLE";
      needUrl = true;
      break;

    case "FAILURE":
      if (currentBuild?.getPreviousBuild()?.resultIsWorseOrEqualTo("FAILURE"))
        status = "NOT FIXED";
      else
        status = "FAILURE";

      needUrl = true;
      break;
  }

  urlText = needUrl ? " <a href=\"${jobUrl}\">Details</a>" : "";
  fullText = "${jobDescription}: <b>${status}</b>.${urlText}";

  if (wantTelegramNotification)
    notifyTelegram(fullText);

  if (status.equals("UNSTABLE"))
    extendedComment = " (This probably means that build against Debian testing fails, which may or may not indicate problem with the commit)";
  else
    extendedComment = "";

  notifyGitlabMrOrCommit("${fullText}${extendedComment}");
}
