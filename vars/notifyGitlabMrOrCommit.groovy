import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;

import hudson.model.Run;
import jenkins.scm.api.SCMRevision;
import jenkins.scm.api.SCMSource;
import jenkins.scm.api.SCMRevisionAction;

import jenkins.plugins.git.GitTagSCMRevision;

import io.jenkins.plugins.gitlabbranchsource.BranchSCMRevision;
import io.jenkins.plugins.gitlabbranchsource.MergeRequestSCMHead;
import io.jenkins.plugins.gitlabbranchsource.MergeRequestSCMRevision;
import io.jenkins.plugins.gitlabbranchsource.GitLabSCMSource;
import io.jenkins.plugins.gitlabbranchsource.helpers.GitLabHelper;

/* The core of the logic comes from the GitLab Branch Source itself. 
   https://github.com/jenkinsci/gitlab-branch-source-plugin/blob/master/src/main/java/io/jenkins/plugins/gitlabbranchsource/helpers/GitLabPipelineStatusNotifier.java

   The plugin is: Copyright (c) 2019 Parichay Barpanda

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
 */

def getSource(Run<?, ?> build) {
    SCMSource s = SCMSource.SourceByItem.findSource(build.getParent());
    if (s instanceof GitLabSCMSource) {
        return (GitLabSCMSource) s;
    }
    return null;
}

def call(String message) {
    Run<?, ?> build = currentBuild.getRawBuild();

    GitLabSCMSource source = getSource(build);
    if (source == null) {
        echo("Huh? Cannot get GitLabSCMSource?");
        return;
    }

    SCMRevision revision = SCMRevisionAction.getRevision(source, build);

    try {
        GitLabApi gitLabApi = GitLabHelper.apiBuilder(build.getParent(), source.getServerName());
        String hash;

        if (revision instanceof BranchSCMRevision) {
            hash = ((BranchSCMRevision) revision).getHash();
            gitLabApi
                .getCommitsApi()
                .addComment(source.getProjectPath(), hash, message);
        } else if (revision instanceof MergeRequestSCMRevision) {
            MergeRequestSCMHead head = (MergeRequestSCMHead) revision.getHead();
            gitLabApi
                .getNotesApi()
                .createMergeRequestNote(
                    source.getProjectPath(),
                    Long.valueOf(head.getId()),
                    message);
        } else if (revision instanceof GitTagSCMRevision) {
            hash = ((GitTagSCMRevision) revision).getHash();
            gitLabApi
                .getCommitsApi()
                .addComment(source.getProjectPath(), hash, message);
        }

    } catch (GitLabApiException | IOException | InterruptedException e) {
        echo("Huh? Exception: ${e}");
    }
}
