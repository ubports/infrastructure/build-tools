#!/bin/bash

# Copyright (C) 2017 Marius Gripsgard <marius@ubports.com>
# Copyright (C) 2024 UBports Foundation
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

basedir=$(dirname "$(readlink -f "$0")")

# Accepts file paths. Print the first file that exists, or nothing if none exists.
first_existing_file () {
  while [ -n "$1" ]; do
    if [ -e "$1" ]; then
      echo "$1"
      return
    fi

    shift
  done
}

sourcedebian_or_source () {
  first_existing_file "source/debian/${1}" "source/${1}"
}

# https://stackoverflow.com/a/8063398
# Usage: contains aList anItem
contains() {
    [[ $1 =~ (^|[[:space:]])$2($|[[:space:]]) ]] && return 0 || return 1
}

get_project_slug() {
  local GIT_URL project_slug

  GIT_URL=$(cd source && git remote get-url origin)

  project_slug=${GIT_URL#https://*/}
  project_slug=${project_slug%.git}

  echo "$project_slug"
}

pr_repo_naming() {
  local GIT_URL GIT_REPO_NAME

  GIT_URL=$(cd source && git remote get-url origin)
  GIT_REPO_NAME="$(basename "${GIT_URL%.git}")"

  echo "PR_${GIT_REPO_NAME}_${CHANGE_ID}"
}

if [ ! "$SKIP_MOVE" = "true" ]; then
        tmp=$(mktemp -d)
        mv ./* ./.* "$tmp/"
        mv "$tmp" ./source
fi

ls source

set -ex

cd source
if [ -f .gitmodules ]; then
  git submodule update --init --recursive
fi
GIT_COMMIT=$(git rev-parse HEAD)
export GIT_COMMIT
export GIT_BRANCH="$BRANCH_NAME"
cd ..

source_location_file=$(sourcedebian_or_source ubports.source_location)
if [ -n "$source_location_file" ]; then
  # All `read` commands in the while loop read from the file redirection
  # at the end of the loop.
  while read -r SOURCE_URL; do
    # If the last line doesn't end with a new line, `read` will return 1,
    # yet the variable will already contain the line's value.
    if ! read -r SOURCE_FILENAME; then
      if [ -n "$SOURCE_FILENAME" ]; then
        echo "WARNING: 'ubports.source_location' doesn't end with a newline character." \
             "This is tolerated, but may produce unexpected result."
      else
        echo "ERROR: 'ubports.source_location' is malformed - URL doesn't have" \
             "corresponding file name."
        exit 1
      fi
    fi

    if [[ "$SOURCE_FILENAME" =~ "/" ]]; then
      echo "ERROR: 'ubports.source_location' is malformed - orig tarball filename" \
            "(\"${SOURCE_FILENAME}\") cannot contain slashes."
      exit 1
    fi

    if [ -f "$SOURCE_FILENAME" ]; then
      rm "$SOURCE_FILENAME"
    fi

    wget -O "$SOURCE_FILENAME" "$SOURCE_URL"
  done <"$source_location_file"

  export IGNORE_GIT_BUILDPACKAGE=true
  # FIXME: This relies on UBports-specific change to generate-git-snapshot.
  # Maybe using PRE_SOURCE_HOOK, but it accepts shell script file path and
  # that means we have to locate the path of ourself.
  export SKIP_PRE_CLEANUP=true

  # Always include the original source in the .changes file.
  # Ideally we should do this only when we have a new upstream version, but
  # I'm too lazy to check if a source package is already in the repo.
  # This is primarily to please Aptly, and Aptly seems to check for file
  # duplication anyway. (see dpkg-genchanges manpage)
  export DBP_EXTRA_OPTS="-sa"

  rm "$source_location_file" || true
  existing_file=$(sourcedebian_or_source Jenkinsfile)
  if [ -n "$existing_file" ]; then
    rm "$existing_file" || true
  fi
fi

# Move files controlling the build process under UBports CI to a directory.
# This prevents dpkg-buildpackage from error out and generate-git-snapshot from
# removing these files during there cleanup process.
mkdir -p buildinfos
for file in \
    ubports.depends \
    ubports.no_test \
    ubports.no_doc \
    ubports.build_profiles \
    ubports.backports \
    ubports.skip_distro \
  ; do
  existing_file=$(sourcedebian_or_source "$file")
  if [ -n "$existing_file" ]; then
    # Move them out of the way so that dpkg-buildpackage won't trip.
    # (And to allow us to inspect the file without extracting debian package
    # in the next step)
    mv "$existing_file" "buildinfos/${file}.buildinfo"
  fi
done

# Skip git cleanup, or those files will come back and/or our modification
# to debian/changelog will be overwritten (see below).
export SKIP_GIT_CLEANUP=true

project_slug=$(get_project_slug)
if [ -n "$CHANGE_TARGET" ]; then
  branch=$CHANGE_TARGET
else
  # Remove branch extension (ubports/focal_-_*) from calculation.
  branch=${GIT_BRANCH%%_-_*}
fi

ut_version_info="${basedir}/ut-version-info.json"

# This jq expression will query all version entries which is applicable for this
# branch. Handling is the same whether it's 1 or multiple entries.
jq -c --arg branch "$branch" '
      to_entries[] |
      { "name": .key } * .value |
      select(.built_from_git_branches[] == $branch)' \
    "$ut_version_info" | \
while read -r dist_info ; do
  dist_name=$(echo "$dist_info" | jq -r .name)
  # TODO: currently, there's no distinction between Debian & Ubuntu base.
  dist_base=$(echo "$dist_info" | jq -r .base_distro.release)
  dist_suffix=$(echo "$dist_info" | jq -r .jdg_distribution_string)
  base_repo=$(echo "$dist_info" | jq -r .aptly_archive)
  allow_repo_extensions=$(echo "$dist_info" | jq -r '.allow_repo_extensions // empty')
  exclude_project_regex=$(echo "$dist_info" | jq -r '.exclude_project_regex // empty')

  if grep -q "^${dist_name}$" buildinfos/ubports.skip_distro.buildinfo; then
    echo "Note: packaging specifies distro '${dist_name}' to be skipped."
    continue
  fi

  if [ -n "$exclude_project_regex" ] && [[ "$project_slug" =~ $exclude_project_regex ]]; then
    echo "Note: distro '${dist_name}' is configured to skip this package."
    continue
  fi

  if [ -n "$CHANGE_TARGET" ]; then
    target_repo="${base_repo}_-_$(pr_repo_naming)"
  elif [[ "$GIT_BRANCH" =~ _-_ ]]; then
    if [ -n "$allow_repo_extensions" ]; then
      target_repo="${base_repo}_-_${GIT_BRANCH#*_-_}"
    else
      echo "Error: branch extension no longer allowed for ${branch}."
      exit 1
    fi
  else
    target_repo=$base_repo
  fi

  echo "Gen git snapshot for ${dist_name}"
  export DIST="$dist_base"
  # FIXME: remove this when we stop using our custom version of `generate-git-snapshot`
  export DIST_OVERRIDE="$DIST"
  # This will be appended to the version number, after a '+'.
  export distribution=$dist_suffix

  # Versioning decision override for PR
  # TODO: might want to refactor this and the version below.
  changelog_dist=$(dpkg-parsechangelog -l source/debian/changelog --show-field Distribution)
  changelog_version=$(dpkg-parsechangelog -l source/debian/changelog --show-field Version)

  if [ "$changelog_dist" = "UNRELEASED" ]; then
    # generate-git-snapsnot does the right thing for unreleased version.
    : This branch intentionally left blank.
  elif [ -n "$CHANGE_TARGET" ]; then
    cd source/
    if git diff --stat "origin/${CHANGE_TARGET}..HEAD" | grep -q '^ debian/changelog '; then
      # A PR is, by definition, prerelease. Add a new changelog entry with UNRELEASED distro so that when
      # the released version comes out (which is when this PR gets merged), this version won't trump the release.
      # generate-git-snapshot will append the timestamp and git commit.
      dch --newversion="$changelog_version" --force-bad-version --distribution=UNRELEASED -- ""
    fi
    cd ../
  fi

  export UNRELEASED_APPEND_COMMIT=true
  generate-git-snapshot
  mkdir -p "mbuild/${dist_name}"
  mv ./*+"${dist_suffix}"* "mbuild/${dist_name}"
  unset DIST
  # FIXME: remove this when we stop using our custom version of `generate-git-snapshot`
  unset DIST_OVERRIDE

  echo "$target_repo" >"mbuild/${dist_name}/ubports.target_apt_repository.buildinfo"
  echo "$dist_name" >> buildinfos/multidist.buildinfo

  # For MRs, support specifying build dependency MRs. This should ideally be
  # read from GitLab's MR dependencies, but for now there's no API for it
  # (yet) [1]. For now, we read ubports.depends.
  #
  # The format is a little different though. We support specifying MRs in form
  # of PR_<repo>_<MR num>  only. We'll prepend 'xenial' or 'focal' for the
  # current multidist target and put it in proper ubports.depends for
  # generate_repo_extra.py to consume.
  #
  # Another catch is that the dependent MR has to also be multidist. If that's
  # not the case, it'll fail in the process of installing dependencies.
  #
  # [1] https://gitlab.com/gitlab-org/gitlab/-/issues/12551

  if [ -e buildinfos/ubports.depends.buildinfo ]; then
    if [ -z "$CHANGE_TARGET" ]; then
      echo "Error: ubports.depends is not supposed to be used for the main branch."
      exit 1
    fi

    while read -r dependency; do
      if ! [[ "$dependency" =~ ^PR_[a-zA-Z0-9_-]+_[0-9]+$ ]]; then
        echo "Error: ubports.depends line is malformed. Don't specify" \
              "distro prefix for main-branch MR. The line is: ${dependency}"
        exit 1 
      fi

      echo "${base_repo}_-_${dependency}" >> "mbuild/${dist_name}/ubports.depends"
    done <buildinfos/ubports.depends.buildinfo
  fi
done
tar -zcvf multidist.tar.gz mbuild

# Move buildinfos back to the workspace root, so that they'll be stashed.
mv buildinfos/* . || true
rm -rf buildinfos || true
